'''
Discord Bot for running a game of Nomic.
By Zach Smith
Version 0.0.1

This bot is designed to facilitate the operation of a
game of Nomic - the game of rules, on a discord server.
It is designed to be stateless: the full game state is
read from the status of the discord server, to enable the
bot to be safely shut down/restarted if needed. 


The bot is intended to interact with several key channels:
    - An "amendments" channel, which stores all currently
        active amendments
    - A "players" channel, which stores information about
        active players
    - A "voting" channel, which stores any currently active
        votes (or information about who has vote privs)
    - A "bot-commands" channel, which is where bot commands
        are processed
    - A "command-info" channel, which is static and just
        lists information about bot commands

It will (eventually) offer the following functionality:
    - Commands to create, modify and remove amendments
    - Commands to change player properties (e.g. points)
    - Automatic resolution of votes after a time,
        (by reading reactions on the vote?)
    - Ability to assign players the ability to create new
        votes
    - Ability to export parts of the current gamestate

Marks (ctrl+f to find):
    - Game Constants
    - Player Commands
    - Amendment Commands
    - Utility Commands
    - Data Parsing
    - Called/Scheduled Tasks
    - Setup
    - Initialization

'''

import asyncio
import time
import sys
import os

import numpy as np
import datetime
import time

import discord
from discord.ext import commands

from dotenv import load_dotenv

import yaml

# Pip3 libraries:
# discord.py asyncio pyyaml numpy python-dotenv future_fstrings

client = commands.Bot(command_prefix="!")

#-----------------------------------------------------------------------------#
#------------------------------- Game Constants ------------------------------#
#-----------------------------------------------------------------------------#

# Looks like a space, but counts as a character
# So discord will not squelch it as if it were whitespace
ZERO_WIDTH_SPACE = '\u200B'
SEC_IN_DAY = 86400

# -- Channel info
AMENDMENTS_CHANNEL = "amendments"
PLAYERINFO_CHANNEL = "player-information"
VOTING_CHANNEL = "voting"
BOTCOMMANDS_CHANNEL = "bot-commands"
COMMANDINFO_CHANNEL = "command-info"

# -- Moderator info
MODERATOR_ROLE_NAME = "Commissioner"

# -- Interesting Paths
TEMP_PATH = "./temp_yaml.txt"
PATH_TO_COMMAND_INFO = "./yml/commands-list.yml"

# Default fields that every player has
PLAYER_FIELDS = [
    { "name": "Points"
      , "default": 0
    }

]

#-----------------------------------------------------------------------------#
#------------------------------ Player Commands ------------------------------#
#-----------------------------------------------------------------------------#

# ---- Meme gnorder
@client.command(name='gnorder')
async def gnorder_command(context):
    cmdChannel = context.message.channel
    await cmdChannel.send("GNORDEEEEER")

# ---- Add Player
@client.command(name='addPlayer')
async def addplayer_command(context, *args):
    # Check for reasons for failure
    # TODO: These should be decorators?
    
    cmdChannel = context.message.channel
    if cmdChannel != client.botCommandsChannel:
        await cmdChannel.send("Bad Channel. Bad!")
        return False
    
    sender = context.message.author
    authorized = False
    for role in sender.roles:
        if role.name == MODERATOR_ROLE_NAME:
            authorized = True
    if not authorized:
        await cmdChannel.send("Not authorized. Bad!")
        return False

    playerName = args[0]
    if (not playerName):
        await cmdChannel.send("No playername. Bad!")
        return False

    embed = await getPlayerEmbedMessageByName(playerName)
    if (embed):
        await cmdChannel.send("Player not found. Bad!")
        return False


    #---

    # Okay, let's actually add a player
    playerEmbed = discord.Embed(
        title = playerName
        , colour = discord.Colour.blue()
    )

    for field in PLAYER_FIELDS:
        playerEmbed.add_field(
            name=field["name"]
            , value=field["default"]
            , inline=False
        )

    await client.playerInfoChannel.send(embed=playerEmbed)


# ---- Remove Player
@client.command(name='removePlayer')
async def removeplayer_command(context, *args):
    # Check for reasons for failure
    # TODO: These should be decorators?
    
    cmdChannel = context.message.channel
    if cmdChannel != client.botCommandsChannel:
        await cmdChannel.send("Bad Channel. Bad!")
        return False
    
    sender = context.message.author
    authorized = False
    for role in sender.roles:
        if role.name == MODERATOR_ROLE_NAME:
            authorized = True
    if not authorized:
        await cmdChannel.send("Not authorized. Bad!")
        return False

    playerName = args[0]
    if (not playerName):
        await cmdChannel.send("No playername. Bad!")
        return False
    
    embed = await getPlayerEmbedMessageByName(playerName)
    if (not embed):
        await cmdChannel.send("Player not found. Bad!")
        return False

    await embed.delete()

# ---- Points to player
@client.command(name="addPoints")
async def givepoints_command(context, *args):
    # Check for reasons for failure
    # TODO: These should be decorators?
    
    cmdChannel = context.message.channel
    if cmdChannel != client.botCommandsChannel:
        await cmdChannel.send("Bad Channel. Bad!")
        return False
    
    sender = context.message.author
    authorized = False
    for role in sender.roles:
        if role.name == MODERATOR_ROLE_NAME:
            authorized = True
    if not authorized:
        await cmdChannel.send("Not authorized. Bad!")
        return False

    playerName = args[0]
    if (not playerName):
        await cmdChannel.send("No playername. Bad!")
        return False

    pointsToAdd = 0
    try:
        pointsToAdd = int(args[1])
    except:
        await cmdChannel.send("Invalid points. Bad!")
        return False
    pointsToAdd = int(args[1])
    
    msg = await getPlayerEmbedMessageByName(playerName)
    if (not msg):
        await cmdChannel.send("Player not found. Bad!")
        return False

    embed = msg.embeds[0]
    fields = embed.fields
    for i in range(len(fields)):
        field = fields[i]
        if field.name == "Points":
            embed.set_field_at(i
                , name=field.name
                , value=str(int(field.value) + pointsToAdd)
            )
    await msg.edit(embed=embed)

# ---- Dump Player yaml
@client.command(name="dumpPlayers")
async def dumpplayers_command(context):
    # Check for reasons for failure
    # TODO: These should be decorators?
    
    cmdChannel = context.message.channel
    if cmdChannel != client.botCommandsChannel:
        await cmdChannel.send("Bad Channel. Bad!")
        return False
    
    sender = context.message.author
    authorized = False
    for role in sender.roles:
        if role.name == MODERATOR_ROLE_NAME:
            authorized = True
    if not authorized:
        await cmdChannel.send("Not authorized. Bad!")
        return False

    players = await getPlayersDict()
    output = yaml.dump(players)
    with open(TEMP_PATH, 'w+') as file:
        file.write(output)
    attachment = discord.File(TEMP_PATH, filename="players.txt")
    await cmdChannel.send(file=attachment)
    os.remove(TEMP_PATH)



#-----------------------------------------------------------------------------#
#----------------------------- Amendment Commands ----------------------------#
#-----------------------------------------------------------------------------#

#-----------------------------------------------------------------------------#
#----------------------------- Utility Commands ------------------------------#
#-----------------------------------------------------------------------------#

# ---- Admin: nuke a channel
@client.command(name='nuke')
@commands.has_permissions(administrator=True)
async def nukeChannel_command(context, *args):    
    cmdChannel = context.message.channel

    channelName = args[0]
    if (not channelName):
        await cmdChannel.send("No channel name. Bad!")
        return False

    for channel in client.guild.channels:
        if channel.name == args[0]:
            await nukeChannel(channel)

# ---- Admin: "reboot" the server
@client.command(name='reboot')
@commands.has_permissions(administrator=True)
async def reboot_command(context):
    await nukeChannel(client.botCommandsChannel)

    await nukeChannel(client.amendmentsChannel)
    await nukeChannel(client.votingChannel)
    await nukeChannel(client.playerInfoChannel)

    await refreshCommandInfoChannel()

#-----------------------------------------------------------------------------#
#-------------------------------- Data Parsing -------------------------------#
#-----------------------------------------------------------------------------#

async def refreshCommandInfoChannel():
    await nukeChannel(client.commandInfoChannel)
    with open(PATH_TO_COMMAND_INFO, 'r', encoding='utf8') as stream:
        yml = yaml.safe_load(stream)
    for command in yml:
        # Okay, let's actually add a player
        commandEmbed = discord.Embed(
            title = command["name"]
            , description = command["description"]
            , colour = discord.Colour.blue()
        )

        commandEmbed.set_footer(text=command["roles"])
        await client.commandInfoChannel.send(embed=commandEmbed)

async def nukeChannel(channel):
    messages = await channel.history().flatten()
    for message in messages:
        await message.delete()

async def getPlayerEmbedMessages():
    messages = await client.playerInfoChannel.history().flatten()
    players = []
    for message in messages:
        if (not message.embeds):
            continue
        players.append(message)
    return players

# Returns a dict of
# playerName: {fields: {points: int}}
async def getPlayersDict():
    playerEmbeds = await getPlayerEmbedMessages()
    players = []
    for message in playerEmbeds:
        playerEmbed = message.embeds[0]
        fields = playerEmbed.fields
        
        player = {}
        player["name"] = playerEmbed.title
        for field in fields:
            if field.name == "Points":
                player["points"] = field.value
        
        players.append(player)
    return players

# Returns a dict of information about a current player
async def getPlayerDictByName(name):
    players = await getPlayersDict()
    for player in players:
        if player.title == name:
            return player

async def getPlayerEmbedMessageByName(name):
    embeds = await getPlayerEmbedMessages()
    for message in embeds:
        playerEmbed = message.embeds[0]
        if playerEmbed.title == name:
            return message
    return False

#-----------------------------------------------------------------------------#
#-------------------------- Called/Scheduled Tasks ---------------------------#
#-----------------------------------------------------------------------------#

# Scheduled task that runs every 60 seconds.
# Just calls a command to spam the chat right now
async def schedule_daily_task():
    await client.wait_until_ready()
    await asyncio.sleep(5) # Dirty hack to make sure on_ready has actually finished running
    await daily_task() # Set it once, just to make sure that e.g. !gotd can activate it after a reset

    # Find out how long until the next minute (e.g.) starts
    timeNow = datetime.datetime.now()
    delta = datetime.timedelta(minutes=1)
    nextMinute = (timeNow + delta).replace(second=0)
    timeToWait = (nextMinute-timeNow).seconds

    await asyncio.sleep(timeToWait)
    while True:
        await daily_task()

        # Calculate next wait time
        timeNow = datetime.datetime.now()
        delta = datetime.timedelta(minutes=1)
        nextMinute = (timeNow + delta).replace(second=0)
        timeToWait = (nextMinute-timeNow).seconds

        # Actually wait
        await asyncio.sleep(timeToWait)

async def daily_task():
    #await client.debugChannel.send("I am a gnome and I live in a dome")
    pass


#-----------------------------------------------------------------------------#
#---------------------------------- Setup ------------------------------------#
#-----------------------------------------------------------------------------#

# Called after the bot first connects to the server
@client.event
async def on_ready():
    for g in client.guilds:
        if g.name == GUILD:
            client.guild = g

    # Initialize channels as variables
    for channel in client.guild.channels:
        if channel.name == PLAYERINFO_CHANNEL:
            client.playerInfoChannel = channel
        if channel.name == VOTING_CHANNEL:
            client.votingChannel = channel
        if channel.name == AMENDMENTS_CHANNEL:
            client.amendmentsChannel = channel
        if channel.name == BOTCOMMANDS_CHANNEL:
            client.botCommandsChannel = channel
        if channel.name == COMMANDINFO_CHANNEL:
            client.commandInfoChannel = channel
#-----------------------------------------------------------------------------#
#---------------------------- Initialization ---------------------------------#
#-----------------------------------------------------------------------------#

# Get hidden environment variables (safety first, kids!)
print("---- Gnome Chomsky Bot Initialise ----")
load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
GUILD = os.getenv('DISCORD_GUILD')
if TOKEN is None:
    sys.exit("---- Tokens failed to load, do you have the .env file? ----")

# This adds an asynchronous task to spam the messages of the day
# I still don't actually understand how it works, but okay.
client.loop.create_task(schedule_daily_task())

# Actually kick off the bot
print("---- Firing up the Bot ----")
client.run(TOKEN)